<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'theme_patisson_description' => 'Thème clair, sobre avec une police de caractère élégante',
	'theme_patisson_slogan' => 'Thème sobre et élégant'
);
